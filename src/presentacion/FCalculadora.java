/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package presentacion;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;
import logica.resultado;

/**
 *
 * @author Estudiante
 */
public class FCalculadora extends JFrame implements ActionListener{
    private resultado res;
    private JPanel pComponentes;
    private JPanel pComponentes1;
    private JTextField resultado;
    private JButton b[][] = new JButton[4][4];

    public FCalculadora() {
        this.res =  new resultado();
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        this.setTitle("Calculadora");
        this.setSize(501, 563);
	this.setLayout(new BorderLayout());
        
        this.pComponentes = new JPanel();
        this.add(this.pComponentes, BorderLayout.CENTER);
        
        this.pComponentes1 =  new JPanel();
        this.add(this.pComponentes1, BorderLayout.NORTH);
        this.resultado = new JTextField("0");
        
        this.pComponentes1.add(this.resultado);
        
        for(int i=0; i<6; i++) {
		for(int j=0; j<7; j++) {
                    this.b[i][j] = new JButton();
                    this.pComponentes.add(this.b[i][j]);
                    this.b[i][j].addActionListener(this);
			}
		}
        this.setVisible(true);
        this.calculadora();
    }
    
    public static void main(String[] args) {
		new FCalculadora();
    }

    
    public void calculadora() {
        for(int i=0; i<4; i++) {
            for(int j=0; j<3; j++) {
		this.b[i][j].setText(String.valueOf(j));
            }
        }
        this.b[0][3].setText("borrar");
        this.b[1][3].setText("/");
        this.b[2][3].setText("X");
        this.b[3][3].setText("+");
        this.b[3][2].setText("-");
        this.b[3][1].setText("=");
    }
    @Override
    public void actionPerformed(ActionEvent e) {
        boolean n = true;
        double r = 0;
        JButton botonOprimido = (JButton)e.getSource();
	String codigo = botonOprimido.getText();
        if(n){
        if(codigo == "1" || codigo == "2" || codigo == "3" || codigo == "4" || codigo == "5" || codigo == "6" || codigo == "7" || codigo == "8" || codigo == "9" || codigo == "0"){
            this.res.setNum1(Integer.parseInt(codigo));
            this.resultado.setText(codigo);
            n = false;
        }
        }else if(codigo == "1" || codigo == "2" || codigo == "3" || codigo == "4" || codigo == "5" || codigo == "6" || codigo == "7" || codigo == "8" || codigo == "9" || codigo == "0"){
            this.res.setNum2(Integer.parseInt(codigo));
            this.resultado.setText(codigo);
        }
        
        if(codigo == "/" || codigo == "X" || codigo == "+" || codigo == "-"){
           this.res.setCod(codigo);
        }else if (codigo == "="){
            r = this.res.operacion();
            this.resultado.setText(String.valueOf(r));
        }else if((codigo == "borrar")){
            this.resultado.setText("");
        }
        
        
    }


    
    
    
    
}
