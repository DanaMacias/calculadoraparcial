/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package logica;

/**
 *
 * @author Estudiante
 */
public class resultado {
    private int matriz[][] = new int[4][4];
    private int num1;
    private int num2;
    private String cod;
     private String cod1;

    public resultado(int num1, int num2, String cod) {
        this.num1 = num1;
        this.num2 = num2;
        this.cod = cod;
        this.cod1 = cod1;
    }
    
    public resultado() {
        this.num1 = 0;
        this.num2 = 0;
        this.cod = "";
        this.cod1 = "";
    }

    public String getCod1() {
        return cod1;
    }

    public void setCod1(String cod1) {
        this.cod1 = cod1;
    }

    public String getCod() {
        return cod;
    }

    public void setCod(String cod) {
        this.cod = cod;
    }
    
    public int[][] getMatriz() {
        return matriz;
    }

    public void setMatriz(int[][] matriz) {
        this.matriz = matriz;
    }

    public int getNum1() {
        return num1;
    }

    public void setNum1(int num1) {
        this.num1 = num1;
    }

    public int getNum2() {
        return num2;
    }

    public void setNum2(int num2) {
        this.num2 = num2;
    }
    
    public double operacion() {
        double op = 0;
        if(this.cod == "/"){
            if(this.num2 != 0){
                op = this.num1 / this.num2;
            }
        }else if(this.cod == "X"){
            op = this.num1 * this.num2;
        }else if(this.cod == "+"){
            op = this.num1 + this.num2;
        }else if(this.cod == "-"){
            op = this.num1 - this.num2;
        }
        return op;
    }
    
    
}
